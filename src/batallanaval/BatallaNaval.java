/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

import java.util.Scanner;

/**
 *
 * @author Sorani
 */
public class BatallaNaval {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Batalla naval = new Batalla();
        Scanner sc = new Scanner(System.in);
        
        System.out.println("¿De qué tamaño será su tablero?");
        int tablero = sc.nextInt();
        int[] Jugador1 = new int[tablero];
        int[] Jugador2 = new int[tablero];
        for (int i = 0; i < tablero; i++){
            Jugador1[i]=0;
            Jugador2[i]=0;
                    
        }
        String[] table = new String[tablero];
        String[] table2 = new String[tablero];
        for (int i = 0; i < tablero; i++) {
            table[i]="O ";
            table2[i]="O ";
        }
        System.out.println("¿Cuántos barcos colocarán?");
        int barcos = sc.nextInt();
        System.out.println("¿Qué nombre lleva el jugador 1?");
        String jugador1 = sc.next();
        System.out.println("¿Qué nombre lleva el jugador 2?");
        String jugador2 = sc.next();
        int contador1 = 0, contador2 = 0;
        do{
            System.out.println(jugador1+" Digita la pocicion de tu barco");
            int posicion = sc.nextInt();
            Jugador1[posicion] = 1;
            contador1++;
        }while (contador1!=barcos);
        do{
            System.out.println(jugador2+" Digita la pocicion de tu barco");
            int posicion = sc.nextInt();
            Jugador2[posicion] = 1;
            contador2++;
        }while (contador2!=barcos);
        System.out.println("Comienza el juego");
        int pt1 = 0, pt2 = 0;
        naval.dibujarTablero(table, table2, tablero);
        do{
            System.out.println(jugador1+" Digita la posicion que deseas atacar");
            int posicion = sc.nextInt();
            if (Jugador2[posicion] == 1) {
                table2[posicion] = "X ";
                pt1++;
            }
            naval.dibujarTablero(table, table2, tablero);
            System.out.println(jugador2+" Digita la posicion que deseas atacar");
            int ataque = sc.nextInt();
            if (Jugador1[ataque] == 1) {
                table[ataque] = "X ";
                pt2++;
            }
            naval.dibujarTablero(table, table2, tablero);
        } while(pt1 < barcos && pt2 < barcos);
        if (pt1 == barcos) {
            System.out.println(jugador1+" ¡Felicidades, ganaste!");
        } else{
            System.out.println(jugador2+" ¡Felicidades, ganaste!");
        }
        
    }
    
}
